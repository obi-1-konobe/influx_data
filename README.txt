1. установка telegraf

wget https://dl.influxdata.com/telegraf/releases/telegraf-1.12.6_linux_amd64.tar.gz
tar xf telegraf-1.12.6_linux_amd64.tar.gz

2. настройка telegraf.conf

3. создание dashboard'ов в grafana
    
    - database status code

    Query: InfluxDB
    SELECT last("database_status_code") FROM "autogen"."http" WHERE ("user" = 'red28') AND $timeFilter GROUP BY time(1m) fill(none)
    visualization: singlestat
    show:current
    thresholds:0.1, 0.9
    0 - OK
    1 - Fail

    - App status code
    
    Query: InfluxDB
    SELECT last("app_status_code") FROM "autogen"."http" WHERE ("user" = 'red28') AND $timeFilter GROUP BY time(1m) fill(none)
    visualization: singlestat
    show:current
    thresholds:0.1, 0.9
    0 - OK
    1 - Fail

    - Flask port responce

    Query: InfluxDB
    SELECT last("result_code") FROM "autogen"."http_response" WHERE ("user" = 'red28' AND "server" = 'http://localhost:5028') AND $timeFilter GROUP BY time($__interval) fill(none)
    visualization: singlestat
    show:current
    thresholds:0.1, 2.9
    0 - Up
    3 - Down

    - flask_red28 status

    Query: InfluxDB
    SELECT last("running") FROM "autogen"."procstat_lookup" WHERE ("user" = 'red28' AND "pidfile" = '/home/red28/frameworks-fullstack/flask.pid') AND $timeFilter GROUP BY time(1m), "pidfile" fill(none)
    visualization: singlestat
    show:current
    thresholds:0.1, 0.9
    1 - Up
    0 - Down

    - RAM load

    Query: InfluxDB
    SELECT last("used_percent") FROM "autogen"."mem" WHERE ("user" = 'red28') AND $timeFilter GROUP BY time($__interval), "host" fill(none)
    visualization: gauge
    show:calculation
    calc:mean
    
    - CPU load

    Query: InfluxDB
    SELECT ( 100 - last("usage_idle")) FROM "autogen"."cpu" WHERE ("user" = 'red28' AND "cpu" = 'cpu-total') AND $timeFilter GROUP BY time($__interval), "host" fill(none)
    visualization: gauge
    show:calculation
    calc:mean

    - Disk usage

    Query: InfluxDB
    SELECT last("used_percent") FROM "autogen"."disk" WHERE ("user" = 'red28') AND $timeFilter GROUP BY time($__interval), "device", "host" fill(none)
    visualization: graph
    
4. Запуск Flask с формированием pid-фаила

flask run --host=0.0.0.0 --port=ПОРТ >> flask.log 2>&1 & echo $! > flask.pid

5. Запуск telegraf с настройками telegraf.conf (в корне)

./telegraf/usr/bin/telegraf --config telegraf.conf

